from django.shortcuts import render_to_response
from KagyuWiki.models import Article


def index(request):
    articles = Article.objects.all()
    context = {'articles': articles}
    return render_to_response('index.html', context)