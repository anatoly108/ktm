from django.contrib import admin
from KagyuWiki.models import Article


class ArticleAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Name', {'fields': ['name']}),
        ('Article', {'fields': ['static_text']}),
        ('Dynamic', {'fields': ['dynamic_content'], 'classes':['collapse']}),
    ]


admin.site.register(Article, ArticleAdmin)