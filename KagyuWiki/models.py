from django.db import models


class Article(models.Model):
    name = models.CharField(max_length=255)
    static_text = models.TextField(max_length=255)
    dynamic_content = models.TextField(max_length=255)
